# Equipe de Tutoriais

## Objetivos:
* Criar tutoriais teórico-práticos úteis para o desenvolvimento do projeto
* Observação: esta equipe é formada por dois grupos, os líderes vão combinar temas de tutoriais de cada grupo, sendo que um será responsável por revisar os tutoriais do outro grupo.
* Requisitos: utilizar jupyter como ferramenta de criação de tutoriais:
    * Exemplos: https://gitlab.com/marceloakira/tutorial

## Grupo 2 - membros (Ps.: Grupo 5 dissolvido e absorvido no 2)
* VITOR DE LIMA CIRQUEIRA (líder)
* AUGUSTO ARAÚJO BORGES
* TIAGO FERREIRA HERMANO
* RODRIGO ARAÚJO
* DÉBORA SILVA
* PEDRO GUSTAVO DIAS DA ROCHA DE ANDRADE
* LUCAS BORGES BUENO DE CAMARGO
* VICTOR MURILO BALBINO MACHADO

## Tarefas da sprint 1 (25/03 a 31/03): 
* Definir tutoriais para cada membro
* Aprender a utilizar jupyter e iniciar tutoriais
# Tutorial configuração NodeJS no Jupyter Notebook usando IJAVASCRIPT

Jupyter Notebooks é uma aplicação web que permite editar, executar e compartilhar código em pyhton nos navegadores. Este tutorial detalha as estapas para instalar e configurar o Jupyter Notebook para suportar Javascript, python2 e python3 ao mesmo tempo.

## Passo 1: Instalar o Python
```
sudo apt-get -y install python python-pip python-dev
```
Depois disso, você deve ter python2, python3, pip2 e pip3 instalado.


## Passo 2: Instalar o Jupyter Notebook

```
pip install jupyter
```

## Passo 3: Adicionando o Kernel Javascript

Pré-requisitos: Node.js instalado

IJavascript poderá ser instalado globalmente com o comando:

```
npm install -g ijavascript
```

Você precisa rodar o IJavascript uma vez para adicionar o kernel js ao Jupyter notebook.

```
ijsinstall
```

Agora, temos todos os kernels necessários instalados.

## Acessando o Kernel Javascript
No diretório correto em que você deseja seus blocos de anotações, execute:

```
jupyter notebook
```

Clique no botão Novo no lado direito e você verá os kernels:

<img src="https://www.sean-lan.com/2016/09/12/jupyter-notebook-set-up/kernel_list.jpeg" width="20%">

<img src="https://www.sean-lan.com/2016/09/12/jupyter-notebook-set-up/javascript.jpeg" width="90%">


